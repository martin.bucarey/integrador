const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class CreateCardEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'CREATE_CARD',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
            number: { type: String, required: true },
            expiration: { type: String, required: true },
            ccv: { type: String, required: true},
            type: { type: String, required: true}
      }
    })
  }
}

module.exports = { CreateCardEvent };
