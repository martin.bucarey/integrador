const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class CreateClientEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'REPORT_CREATE_CLIENT',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        id: { type: String, required: true },
        dni: { type: String, required: true },
        name: { type: String, required: true },
        lastName: { type: String, required: true },
        birth: { type: String, required: true },
      },
    })
  }
}

module.exports = { CreateClientEvent };