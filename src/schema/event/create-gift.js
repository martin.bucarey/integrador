const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class CreateGiftEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'CREATE_GIFT',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        Gift: { type: String, required: true }
      }
    })
  }
}

module.exports = { CreateGiftEvent };
