const { InputValidation } = require('ebased/schema/inputValidation');

class PurchaseInputSchema extends InputValidation {
    constructor(payload,meta){
        super({
            source: meta.status,
            payload: payload,
            source: 'PURCHASE.CREATE',
            specversion: 'v1.0.0',
            schema: {
                strict: false,
                id: { type: String, required: true },
                clientDni: { type: String, required: true },
                products:{ type : [{
                        name: String,
                        originalPrice:Number
                    }]
                }
            }
        })
    }
}

module.exports = { PurchaseInputSchema };