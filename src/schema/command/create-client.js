const { InputValidation } = require('ebased/schema/inputValidation');

class CreateClientInputSchema extends InputValidation {
    constructor(payload,meta){
        super({
            source: meta.status,
            payload: payload,
            source: 'CLIENT.CREATE',
            specversion: 'v1.0.0',
            schema: {
                strict: false,
                id: { type: String, required: true },
                dni: { type: String, required: true },
                name: { type: String, required: true },
                lastName: { type: String, required: true },
                birth: { type: String, required: true },
                active: { type: Boolean, required: true},
                points : { type: Number, required: true }
            }
        })
    }
}

module.exports = { CreateClientInputSchema };