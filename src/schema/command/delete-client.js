const { InputValidation } = require('ebased/schema/inputValidation');

class deleteClientInputSchema extends InputValidation {
    constructor(payload, meta) {
      super({
        source: meta.status,
        payload: payload,
        source: 'CLIENT.DELETE',
        specversion: 'v1.0.0',
        schema: {
          strict: false,
          id: { type: String, required: true }
        },
      });
    }
  };

  module.exports = { deleteClientInputSchema };