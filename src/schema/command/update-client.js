const { InputValidation } = require('ebased/schema/inputValidation');

class UpdateClientInputSchema extends InputValidation {
    constructor(payload,meta){
        super({
            source: meta.status,
            payload: payload,
            source: 'CLIENT.UPDATE',
            specversion: 'v1.0.0',
            schema: {
                strict: false,
                id: { type: String, required: false },
                dni: { type: String, required: false },
                name: { type: String, required: false },
                lastName: { type: String, required: false },
                birth: { type: String, required: false }
            },
        })
    }
}

module.exports = { UpdateClientInputSchema };