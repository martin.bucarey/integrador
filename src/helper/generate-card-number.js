const generateNumber = () => {
    const min = 0000;
    const max = 9999;
    return `${Math.floor(Math.random()*(max-min+1)+min)} - ${Math.floor(Math.random()*(max-min+1)+min)} - ${Math.floor(Math.random()*(max-min+1)+min)} - ${Math.floor(Math.random()*(max-min+1)+min)}`
}

module.exports = { generateNumber }