const generateGift = (birthDay, birthMonth) => {
    let giftBirthday ='';
        if( birthMonth >=1 && birthMonth <= 3){
            if( birthDay >= 21 && birthMonth===3){
                giftBirthday = 'Buzo';
            } else {
                giftBirthday = 'Remera';
            }
        } else if( birthMonth >= 3 && birthMonth <=6){
            if( birthDay >= 21 && birthMonth===6){
                giftBirthday = 'Sweater';
            } else {
                giftBirthday = 'Buzo';
            }
        } else if ( birthMonth >= 6 && birthMonth <=9) {
             if( birthDay >= 21 && birthMonth===9){
                giftBirthday = 'Camisa';
            } else {
                giftBirthday = 'Sweater';
            }
        } else if( birthMonth >= 9 && birthMonth <= 12 ) {
            if( birthDay >= 21 && birthMonth===12){
                giftBirthday = 'Remera';
            } else {
                giftBirthday = 'Camisa';
            }
        }
    return giftBirthday;
}

module.exports = { generateGift };