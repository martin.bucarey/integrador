const { eventMapper } = require('ebased/handler');
const inputMode = require('ebased/handler/input/eventTopic');
const outputMode = require('ebased/handler/output/eventConfirmation');

const createClientDomain = require('../domain/reportExchange');

module.exports.handler = async (command, context) => {
    return commandMapper({ command, context }, inputMode, createClientDomain, outputMode);
  }