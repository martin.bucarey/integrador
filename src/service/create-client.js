const dynamo = require('ebased/service/storage/dynamo');
const sns = require('ebased/service/downstream/sns');
const sqs = require('ebased/service/downstream/sqs');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');
const TOPIC_CREATE_CLIENT = config.get('TOPIC_CREATE_CLIENT_TABLE');
const SQS_CREATE_CARD = config.get('QUEUE_CREATE_CARD');
const SQS_CREATE_GIFT = config.get('QUEUE_CREATE_GIFT');

const createClient = async (item) => {
    await dynamo.putItem({ TableName: TABLE_NAME, Item: item });
}

const reportCreateClient = async (clientEvent) =>{
    const { eventPayload, eventMeta } = clientEvent.get();
    const snsPublishParams = {
        TopicArn: TOPIC_CREATE_CLIENT,
        Message: eventPayload,
    };
    await sns.publish(snsPublishParams, eventMeta);
    const sqsSendParamsCard = {
        QueueUrl: SQS_CREATE_CARD,
        MessageBody: eventPayload,
    }
    await sqs.send(sqsSendParamsCard, eventMeta);
    const sqsSendParamsGift = {
        QueueUrl: SQS_CREATE_GIFT,
        MessageBody: eventPayload,
    }
    await sqs.send(sqsSendParamsGift, eventMeta);
}

module.exports = { createClient, reportCreateClient };