const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');

const deleteClient = async (client) => {
    const paramsUpdate = {
        ExpressionAttributeNames: {
            "#A": "active",
        },
        ExpressionAttributeValues: {
            ":a": false
        }, 
        Key:{
            "id": client.id
        },
        ReturnValues: "ALL_NEW",
        TableName: TABLE_NAME,
        UpdateExpression: "SET #A = :a",
    };
    const { Item } = await dynamo.updateItem(paramsUpdate);
    return Item;  
}

module.exports = { deleteClient };