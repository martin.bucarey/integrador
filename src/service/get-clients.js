const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');

const getClients = async () => {
    const { Items } = await dynamo.scanTable({
        TableName: TABLE_NAME});
    return Items;
}

module.exports = { getClients };