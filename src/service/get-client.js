const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');

const getClient = async (client) => {
    const { Item } = await dynamo.getItem({
        TableName: TABLE_NAME,
        Key:{ "id": client.id }
      });
    return Item;  
}

module.exports = { getClient };