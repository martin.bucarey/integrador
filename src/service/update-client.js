const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');
const { reportCreateClient } = require('./create-client');
const { CreateClientEvent } = require('../schema/event/report-create-client');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');

const updateClient = async (client, commandMeta) => {
    let changeBirth = false;
    const params = {
        TableName: TABLE_NAME,
        Key: { "id": client.id },
        UpdateExpression: 'SET',
        ExpressionAttributeNames: {},
        ExpressionAttributeValues: {},
        ReturnValues: 'ALL_NEW',
      }
      Object.keys(client).forEach(async key => {
        if(key!=='id') {
            params.UpdateExpression += ` #${key}=:${key},`;
            params.ExpressionAttributeNames[`#${key}`] = key;
            params.ExpressionAttributeValues[`:${key}`] = client[key];
        }
        if(key === 'birth'){
          changeBirth = true;
        }
      });
      params.UpdateExpression = params.UpdateExpression.slice(0, -1);
      const { Attributes } = await dynamo.updateItem(params);
      if(changeBirth) await reportCreateClient(new CreateClientEvent(Attributes, commandMeta));
      return Attributes;  
}

module.exports = { updateClient };