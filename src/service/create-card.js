const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');


const createCard = async (creditCard, clientId) => {
    const paramsUpdate = {
        ExpressionAttributeNames: {
            "#C": "creditCard",
        },
        ExpressionAttributeValues: {
            ":c": {
                "number": creditCard.number,
                "expiration": creditCard.expiration,
                "ccv": creditCard.ccv,
                "type": creditCard.type
            },
        }, 
        Key:{
            "id": clientId
        },
        ReturnValues: "ALL_NEW",
        TableName: TABLE_NAME,
        UpdateExpression: "SET #C = :c",
    };
    await dynamo.updateItem(paramsUpdate)
}

module.exports = { createCard };