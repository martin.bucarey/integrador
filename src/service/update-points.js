const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME_CLIENT = config.get('CREATE_CLIENT_TABLE');

const updatePoints = async (client, pointsAcc) => { 
    const actualPoints = client.points + pointsAcc;
    const paramsUpdate = {
        ExpressionAttributeNames: {
            "#P": "points",
        },
        ExpressionAttributeValues: {
            ":p": actualPoints
        }, 
        Key:{
            "id": client.id
        },
        ReturnValues: "ALL_NEW",
        TableName: TABLE_NAME_CLIENT,
        UpdateExpression: "SET #P = :p",
    };
    await dynamo.updateItem(paramsUpdate);
}

module.exports = { updatePoints };