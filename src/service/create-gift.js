const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CREATE_CLIENT_TABLE');

const createGift = async (giftBirthday, clientId) => {
    const paramsUpdate = {
        ExpressionAttributeNames: {
            "#G": "Gift",
        },
        ExpressionAttributeValues: {
            ":g": giftBirthday
        }, 
        Key: {
            "id": clientId
        },
        ReturnValues: "ALL_NEW",
        TableName: TABLE_NAME,
        UpdateExpression: "SET #G = :g",
    };
    await dynamo.updateItem(paramsUpdate)
}

module.exports = { createGift };