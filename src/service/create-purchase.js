const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');
const { ErrorHandled } = require('ebased/util/error');

const { updatePoints } = require('./update-points');

const TABLE_NAME = config.get('CREATE_PURCHASE_TABLE');
const TABLE_NAME_CLIENT = config.get('CREATE_CLIENT_TABLE');

const createPurchase = async (purchase) => {
    const { Item } = await dynamo.getItem({TableName: TABLE_NAME_CLIENT, Key:{ "id": purchase.clientDni }});
    if(Item.active){
        const { products } = purchase;
        let pointsAcc=0;
        products.map((product) =>{
            product['finalPrice'] = Item.creditCard.type === 'Gold' ? product.originalPrice * 0.88 : product.originalPrice * 0.92 ;
            pointsAcc += product.finalPrice / 200;
            return product;
        })
        await dynamo.putItem({ TableName: TABLE_NAME, Item: purchase });
        await updatePoints(Item, pointsAcc);        
    } else {
         throw new ErrorHandled('Cliente inactivo', { status:404, code: 'NOT_FOUND' });
    }
    
}

module.exports = { createPurchase };