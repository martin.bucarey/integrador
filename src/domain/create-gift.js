const moment = require('moment');
const { generateGift } = require('../helper/generate-gift');
const { createGift } = require('../service/create-gift');
const { CreateGiftEvent } = require('../schema/event/create-gift');

 module.exports = async (eventPayload, eventMeta) => {
    const birthMonth = moment(eventPayload.birth).format('M');
    const birthDay = moment(eventPayload.birth).format('D');
    const giftBirthday = generateGift(birthDay, birthMonth);
    new CreateGiftEvent({ 'Gift':giftBirthday }, eventMeta);
    try {
        await createGift(giftBirthday, eventPayload.id);
    } catch(error){
        return {
            statusCode : 500,
            body: error
        }
    }
    const response = {
        statusCode: 200,
        body: 'Regalo creado',
    };
    return response;
 }
