const { getClientInputSchema } = require('../schema/command/get-client');
const { getClient } = require('../service/get-client');

module.exports = async (commandPayload, commandMeta) => {  
    const client = new getClientInputSchema(commandPayload, commandMeta).get();
    const gettedClient = await getClient(client);
    return { status: 200, body: gettedClient };
}