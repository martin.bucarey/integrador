const uuid = require('uuid');
const { enabledClient } = require('../helper/enable-client');
const { createClient, reportCreateClient } = require('../service/create-client');
const { CreateClientInputSchema } = require('../schema/command/create-client');
const { CreateClientEvent } = require('../schema/event/report-create-client');

 module.exports = async (commandPayload, commandMeta)=>{
    commandPayload.id = uuid.v1();
    commandPayload.active = true;
    commandPayload.points = 0;
    const client = new CreateClientInputSchema(commandPayload, commandMeta).get();
    const {id, dni, name, lastName, birth} = commandPayload;
    const eventClient = {
        id: id,
        dni: dni,
        name: name,
        lastName: lastName,
        birth: birth
    };
    if( enabledClient(birth) ){
      await createClient(client);
      await reportCreateClient(new CreateClientEvent(eventClient, commandMeta));
    } else {
         return {
             statusCode: 400,
             body: 'El cliente no cumple con las restricciones de edad'
         }
    }
    const response = {
        statusCode: 200,
        body: 'Cliente creado',
    };
    return response;
 }
