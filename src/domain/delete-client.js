const { deleteClientInputSchema } = require('../schema/command/delete-client');
const { deleteClient } = require('../service/delete-client');

module.exports = async (commandPayload, commandMeta) => {  
    const client = new deleteClientInputSchema(commandPayload, commandMeta).get();
    const deletedClient = await deleteClient(client);
    return { status: 200, body: deletedClient };
}