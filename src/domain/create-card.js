const moment = require('moment');   
const { generateNumber } = require('../helper/generate-card-number');
const { CreateCardEvent } = require('../schema/event/create-card');
const { createCard } = require('../service/create-card');


module.exports = async (eventPayload, eventMeta) => {
    const age = moment().diff(moment(eventPayload.birth),'years');
    const creditCard = {
        number : generateNumber(),
        expiration : `${moment().add(4, 'years').format('MM-YY')}`,
        ccv : `${Math.floor(Math.random()*(999-000+1)+000)}`,
        type:  age < 45 ? 'Classic' : 'Gold'
    };
    new CreateCardEvent(creditCard, eventMeta).get();
    try{
        await createCard(creditCard, eventPayload.id);
    } catch(error){
        return {
            statusCode: 500,
            body: error
        }
    }
    const response = {
        statusCode: 200,
        body: 'Tarjeta creada',
    };
    return response;
}
