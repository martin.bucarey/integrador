const uuid = require('uuid');
const { PurchaseInputSchema } = require('../schema/command/purchase');
const { createPurchase } = require('../service/create-purchase');

module.exports = async (commandPayload, commandMeta) => {
    commandPayload.id = uuid.v1();
    new PurchaseInputSchema(commandPayload, commandMeta);
    const { id, clientDni, products } = commandPayload;
    try{
        await createPurchase({ id, clientDni, products });
    } catch(error) {
        return error;
    }
}