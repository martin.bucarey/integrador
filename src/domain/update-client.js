const { UpdateClientInputSchema } = require('../schema/command/update-client');
const { updateClient } = require('../service/update-client');

module.exports = async (commandPayload, commandMeta) => {  
    const client = new UpdateClientInputSchema(commandPayload, commandMeta).get();
    const updatedClient = await updateClient(client, commandMeta);
    return { status: 200, body: updatedClient };
}