const { getClients } = require('../service/get-clients');

module.exports = async () => {  
    const gettedClients = await getClients();
    return { status: 200, body: gettedClients };
}